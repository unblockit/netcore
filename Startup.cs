using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using unblock_it_dotnetcore.Tools;
using unblock_it_dotnetcore.Services;
using unblock_it_dotnetcore.Health;
using Newtonsoft.Json.Linq;
using System.Linq;
using Newtonsoft.Json;
using Prometheus;
using System.Net;
using unblock_it_dotnetcore.Tasks;
using unblock_it_dotnetcore.Collectors;
using unblock_it_dotnetcore.Extensions;

namespace unblock_it_dotnetcore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var section = Configuration.GetSection(nameof(UnBlockITDatabaseSettings));

            // requires using Microsoft.Extensions.Options
            services.Configure<UnBlockITDatabaseSettings>(section);

            var unBlockITDatabaseSettings = section.Get<UnBlockITDatabaseSettings>();

            services.AddHealthChecks()
                .AddMemoryHealthCheck("memory", tags: new string[] { "ready" })
                .AddMongoDb(name: "mongo", tags: new string[] { "ready" },
                    mongodbConnectionString: unBlockITDatabaseSettings.ConnectionString);

            services.AddSingleton<IUnBlockITDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<UnBlockITDatabaseSettings>>().Value);

            services.AddSingleton<WeatherForecastService>();

            services.AddControllers();

            services.AddHostedService<ServiceMetricsHostedService>();
            services.AddServiceMetricCollector<MongoStatusProbe>();

        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            //app.UseMetricServer();
            app.UseAuthorization();

            app.UseHttpMetrics();
            app.UseHttpsRedirection();

            var counter = Metrics.CreateCounter("http_server_requests_seconds_count",
                "http_server_requests_seconds", new CounterConfiguration
                {
                    LabelNames = new[] { "method", "outcome", "status", "uri" }
                });

            app.Use((context, next) =>
            {
                counter.WithLabels(context.Request.Method,
                                  ((HttpStatusCode)context.Response.StatusCode).ToString(),
                                   context.Response.StatusCode.ToString(),
                                   context.Request.Path).Inc();
                return next();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                endpoints.MapHealthChecks("/health", new HealthCheckOptions()
                {
                    // This custom writer formats the detailed status as JSON.
                    ResponseWriter = WriteResponse
                });

                // The readiness check uses all registered checks with the 'ready' tag.
                endpoints.MapHealthChecks("/health/ready", new HealthCheckOptions()
                {
                    Predicate = (check) => check.Tags.Contains("ready"),
                });

                endpoints.MapHealthChecks("/health/live", new HealthCheckOptions()
                {
                    // Exclude all checks and return a 200-Ok.
                    Predicate = (_) => false
                });

                endpoints.MapMetrics();

            });
        }

        private static Task WriteResponse(HttpContext httpContext, HealthReport result)
        {
            httpContext.Response.ContentType = "application/json";

            var json = new JObject(
                new JProperty("status", result.Status.ToString()),
                new JProperty("results", new JObject(result.Entries.Select(pair =>
                    new JProperty(pair.Key, new JObject(
                        new JProperty("status", pair.Value.Status.ToString())))))));

            return httpContext.Response.WriteAsync(
                json.ToString(Formatting.Indented));
        }
    }
}
