using Microsoft.Extensions.DependencyInjection;
using unblock_it_dotnetcore.Collectors;

namespace unblock_it_dotnetcore.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServiceMetricCollector<T>(this IServiceCollection services)
            where T : class, IServiceMetricCollector
        {
            return services.AddSingleton<IServiceMetricCollector, T>();
        }

    }
}