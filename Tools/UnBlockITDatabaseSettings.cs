using System;

namespace unblock_it_dotnetcore.Tools
{
    public class UnBlockITDatabaseSettings : IUnBlockITDatabaseSettings
    {
        public string CollectionName => "weatherforecast";
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
    public interface IUnBlockITDatabaseSettings
    {
        string CollectionName { get; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
