using System.Collections.Generic;
using MongoDB.Driver;
using unblock_it_dotnetcore.Models;
using unblock_it_dotnetcore.Tools;

namespace unblock_it_dotnetcore.Services
{
    public class WeatherForecastService
    {

        private readonly IMongoCollection<WeatherForecast> _WeatherForecasts;

        public WeatherForecastService(IUnBlockITDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _WeatherForecasts = database.GetCollection<WeatherForecast>(settings.CollectionName);
        }

        public List<WeatherForecast> Get() =>
            _WeatherForecasts.Find(WeatherForecast => true).ToList();

        public WeatherForecast Get(string id) =>
            _WeatherForecasts.Find<WeatherForecast>(WeatherForecast => WeatherForecast.Id == id).FirstOrDefault();

        public WeatherForecast Create(WeatherForecast WeatherForecast)
        {
            _WeatherForecasts.InsertOne(WeatherForecast);
            return WeatherForecast;
        }

        public void Update(string id, WeatherForecast WeatherForecastIn) =>
            _WeatherForecasts.ReplaceOne(WeatherForecast => WeatherForecast.Id == id, WeatherForecastIn);

        public void Remove(WeatherForecast WeatherForecastIn) =>
            _WeatherForecasts.DeleteOne(WeatherForecast => WeatherForecast.Id == WeatherForecastIn.Id);

        public void Remove(string id) =>
            _WeatherForecasts.DeleteOne(WeatherForecast => WeatherForecast.Id == id);
    }

}
