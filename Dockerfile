# https://hub.docker.com/_/microsoft-dotnet-core
# base image from dotnetcore alpine
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine

LABEL maintainer="Ismael Queiroz <eu@queiroz.dev>" \
  org.label-schema.name="Unblock IT (dotnetcore)" \
  org.label-schema.vendor="Observability & Monitoring" \
  org.label-schema.schema-version="1.0.0-RC"

# default to timeZone
ENV TZ America/Sao_Paulo

# default to UTF-8 file.encoding
ENV LC_ALL pt_BR.UTF-8
ENV LANG pt_BR.UTF-8

# add libs
RUN apk add --no-cache icu-libs curl

## set /app directory as default working directory
WORKDIR /app

## copy all file from current dir to /app in container
COPY ./build .

## expose port 80
EXPOSE 80

## cmd to start service
CMD ["dotnet", "unblock-it-dotnetcore.dll"]