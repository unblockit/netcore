using Prometheus;

namespace unblock_it_dotnetcore.Collectors
{
    public interface IServiceMetricCollector
    {

        bool IsSupported { get; }

        void CreateMetrics(MetricFactory factory);

        void UpdateMetrics();
    }
}