using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace unblock_it_dotnetcore.Models
{
    public class WeatherForecast
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("date")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Date { get; set; }

        [BsonElement("temperatureC")]
        public int TemperatureC { get; set; }

        [BsonElement("temperatureF")]
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        [BsonElement("summary")]
        public string Summary { get; set; }
    }
}
