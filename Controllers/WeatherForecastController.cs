﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using unblock_it_dotnetcore.Models;
using unblock_it_dotnetcore.Services;

namespace unblock_it_dotnetcore.Controllers
{
    [ApiController]
    [Route("api/wf")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        private readonly WeatherForecastService _weatherForecastService;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, WeatherForecastService weatherForecastService)
        {
            _logger = logger;

            _weatherForecastService = weatherForecastService;
        }

        [HttpGet]
        public ActionResult<List<WeatherForecast>> Get() =>
            _weatherForecastService.Get();

        [HttpGet("{id:length(24)}", Name = "GetWeatherForecast")]
        public ActionResult<WeatherForecast> Get(string id)
        {
            var WeatherForecast = _weatherForecastService.Get(id);

            if (WeatherForecast == null)
            {
                return NotFound();
            }

            return WeatherForecast;
        }

        [HttpPost]
        public ActionResult<WeatherForecast> Create(WeatherForecast WeatherForecast)
        {
            _weatherForecastService.Create(WeatherForecast);

            return CreatedAtRoute("GetWeatherForecast", new { id = WeatherForecast.Id.ToString() }, WeatherForecast);
        }

    }
}
